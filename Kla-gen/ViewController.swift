//
//  ViewController.swift
//  Kla-gen
//
//  Created by Hamzah Al Farisi on 15/11/18.
//  Copyright © 2018 Hamzah Al Farisi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.layer.cornerRadius = 5;
        view.layer.masksToBounds = true;
        
    }


}

